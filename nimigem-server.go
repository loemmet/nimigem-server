package main

import (
	"crypto/sha256"
	"crypto/tls"
	"encoding/hex"
	"fmt"
	flag "github.com/spf13/pflag"
	"io/ioutil"
	"log"
	"net"
	"net/url"
	"os"
	"strings"
	"time"

	gemini "github.com/LukeEmmet/net-gemini"
)

var version = "0.1.3"
var serverName = "Nimigem-Server"
var upSince = time.Now().Format(time.ANSIC)

//All of this server state is discarded when the server is restarted - no persistence for this demo
var messageOfTheDay = "Show kindness to strangers and small animals"
var messages  = []string {}
var votes = make(map[string]int)		//counts the vots by fingerprint
var footer = `



__________________ __________________ __________________ __________________ 

=> / Home
`
var (
	 root = flag.StringP("root", "r", "docs", "root directory")
	 cgi = flag.StringP("cgi", "c", "cgi-bin", "cgi directory")
	 crt = flag.StringP("crt", "", "", "path to PEM certificate")
	 key = flag.StringP("key", "", "", "path to PEM private key for certificate")
	 bind = flag.StringP("bind", "b", ":1965", "bind to")
)



//geminiQuote takes a string and marks it up as a gemini quotation
func geminiQuote(s string) string {
	out := s
	out = strings.Replace(out, "\n" , "\n> ", -1)
	return ("> " + out)
}

//gemtextPreformatEscape escapes any preformat markers with a leading zero width space so they dont break out of the preformatted/edited region
func gemtextPreformatEscape(s string) string {
	const ZWSP = '\u200B'
    
    out := "\n" + s
	out = strings.Replace(out, "\n```", "\n" + string(ZWSP) + "```", -1 )
	return out[1:]	//strip the added newline from above
}


//messageBoard implements a simple append-only message board feature. Displays the list of messages and allows the visitor
//to add another one, posting using nimigem. Currently has no persistence, so reset when server restarts
func messageBoard(w *gemini.Response, r *gemini.Request , verb string) {

	gmi := "# A Simple message board with Nimigem\n\n" +
		"A simple message board, using the Nimigem protocol. Scroll to the bottom to post your message.\n\n" +
		"Requires a Nimigem compatible client. Note that the usernames used are self-reported and not verified.\n\n" +
		"Note that all messages are discarded once the server is restared.\n\n"
	switch verb {
	case "post":
		//handle nimigem payload
		username := "Anonymous"
		if r.URL.RawQuery != "" {
			username, _ = url.PathUnescape(r.URL.RawQuery)
		}
		messages = append(messages, "" +
			"### " + username + ", "+ time.Now().Format(time.ANSIC) + "\n\n" +
			geminiQuote(string(r.Payload)))

		w.SetStatus(gemini.StatusNimigemSuccess, "gemini://" + r.URL.Host + "/board?" + r.URL.RawQuery)		//redirects to show the resource
		return

	case "show":
		if r.URL.RawQuery == "" {
			w.SetStatus(gemini.StatusInput, "Please provide your username to post as")
			return
		}

		msgGmi := ""
		if len(messages) == 0 {
			msgGmi = "No messages yet, please post one :)"
		} else {
			for _, message := range messages {
				msgGmi += message + "\n\n"
			}
		}

		gmi += "## Current messages\n\n" +
			msgGmi + "\n\n" +
			"## Post a new message\n\n" +
			"```✏️ write a message\n" +       //✏️ pencil lead character U+270F indicates range is editable and may be bound to a subsequent nimigem link
			"```\n\n" +
			"=> nimigem://" + r.URL.Host + "/board/post?" + r.URL.RawQuery + " Post message\n\n"
		break
	}

	w.SetStatus(gemini.StatusSuccess, "text/gemini")
	w.Write(([]byte(gmi + footer)))
}

func check(e error) {
	if e != nil {
		fmt.Fprintf(os.Stderr, "%s", e)
		os.Exit(1)
	}
}

func saveFile(contents []byte, path string) {
	d1 := []byte(contents)
	err := ioutil.WriteFile(path, d1, 0644)
	check(err)
}

//uploads accepts file posts and stores them with a unique name
func uploads(w *gemini.Response, r *gemini.Request , verb string) {

	gmi := "# Uploads\n\n" +
		"A demonstration of using the Nimigem protocol to post files.\n\n" +
		"Requires a Nimigem compatible client.\n\n"

	switch verb {
	case "post":
		//handle nimigem payload
		location := "uploads/save.bin"

		//gemini.Debug(string(r.Payload))

		saveFile(r.Payload, location)
		w.SetStatus(gemini.StatusNimigemSuccess, "gemini://" + r.URL.Host + "/uploads/show" )		//redirects to show the resource
		return

	case "show":
		gmi +=  "list of files TBD\n"
		break
    }
    
    w.SetStatus(gemini.StatusSuccess, "text/gemini")
	w.Write(([]byte(gmi + footer)))

}

//messageOTD displays or updates the global message of the day on the server. Uses nimigem for posting content.
//Curren	tly anyone can update it, but could be adapted to use certificates to restrict access.
//Currently has no persistence, so reset when server restarts
func messageOTD(w *gemini.Response, r *gemini.Request , verb string) {

	gmi := "# Message of the day\n\n" +
		"A demonstration of using the Nimigem protocol to edit a server message. This message can be edited by any visitor at present.\n\n" +
		"Requires a Nimigem compatible client.\n\n"

	switch verb {
	case "post":
		//handle nimigem payload
		messageOfTheDay = string(r.Payload)
		w.SetStatus(gemini.StatusNimigemSuccess, "gemini://" + r.URL.Host + "/motd")		//redirects to show the resource
		return

	case "show":
		gmi +=  geminiQuote(messageOfTheDay) + "\n" +
			"\n" +
			"=> /motd/edit Edit this message\n"
		break

	case "edit":
		gmi += "```✏️ edit the message of the day\n" +     //✏️ pencil lead character U+270F indicates range is editable and may be bound to a subsequent nimigem link
			gemtextPreformatEscape(messageOfTheDay) + "\n" +		//escape any preformat markers within the region
			"```\n" +
			"\n" +
			"=> nimigem://" + r.URL.Host + "/motd/post Post this message"
		break
	}

	w.SetStatus(gemini.StatusSuccess, "text/gemini")
	w.Write(([]byte(gmi + footer)))
}

func certPage(w *gemini.Response, r *gemini.Request) {

	var conn net.Conn
	conn = w.Conn

	//vars := make(map[string]string)
	// Add TLS variables
	var tlsConn (*tls.Conn) = conn.(*tls.Conn)
	connState := tlsConn.ConnectionState()
	//	vars["TLS_CIPHER"] = CipherSuiteName(connState.CipherSuite)

	// Add client cert variables
	clientCerts := connState.PeerCertificates
	if len(clientCerts) > 0 {
		cert := clientCerts[0]
		hash := sha256.Sum256(cert.Raw)
		fingerprint := hex.EncodeToString(hash[:])


		if r.URL.Scheme == "nimigem" {
			//handle the actions
			if r.URL.RawQuery == "raise"{
				votes[fingerprint] += 1
			}

			if r.URL.RawQuery == "lower"{
				votes[fingerprint] -= 1
			}

			w.SetStatus(25, "gemini://" + r.URL.Host + "/certs")
			return
		}

		//show the gemini response

		w.SetStatus(20, "text/gemini")

		w.Write([]byte("" +
			"# Page demonstrating use of Nimigem with client certificates\n\n" +
			"Vote yourself up and down using nimigem. This demonstrates use of Nimigem with no text payload\n\n" +
			""))

		//show the certificate status
		w.Write([]byte("" +
			"## Certificate status\n\n" +
			"You provided a certificate:\n\n" +
			"```\n\n" +
			"fingerprint: " + fingerprint + "\n" +
			"issuer: " + cert.Issuer.String() + "\n" +
			"common name: " + cert.Issuer.CommonName + "\n" +
			"issuer: " + cert.Issuer.String() + "\n" +
			"subject: " + cert.Subject.String() + "\n" +
			"subject common name: " + cert.Subject.CommonName + "\n" +
			"```\n\n" +

			""))

		//show the vote count and voting mechanism
		w.Write([]byte( "" +
			fmt.Sprintf("## Personal votes: %d", votes[fingerprint]) + "\n\n" +
			"Activate the following nimigem links to raise or lower your personal \"vote\", for yourself. " +
			"If you connect with a different certificate, that counter is shown\n\n" +
			"=> nimigem://" + r.URL.Host + "/certs?raise Vote myself up +\n" +
			"=> nimigem://" + r.URL.Host + "/certs?lower Vote myself down -\n" +
			""))

		w.Write([]byte(footer))


	} else {
		w.SetStatus(60, "Please provide a client certificate")
		w.Write([]byte("You have no certs\n"))
	}
}

//infoPage provides a display of some basic server info
func infoPage(w *gemini.Response, r *gemini.Request) {
	info := "# Server info\n" +
		"\n" +
		"* server name: " + serverName + "\n" +
		"* version: " + version + "\n" +
		"* serving on: " + r.URL.Host + "\n" +
		"* docs folder: " + *root + "\n" +
		"* cgi folder: " + *cgi + "\n" +
		"* up since: " + upSince + "\n" +
		"* local time: " + time.Now().Format(time.ANSIC) + "\n" +
		""

	w.SetStatus(gemini.StatusSuccess, "text/gemini")
	w.Write(([]byte(info + footer)))
}

func main() {
	flag.Parse()

	fmt.Fprintln(os.Stderr, "Starting up " + serverName + " v" + version + " on " + *bind)

	//use cgi module to handle urls starting cgi-bin
	gemini.Handle("/cgi-bin", gemini.CGIServer(*cgi, serverName))

    //handle /info with specific function 
    gemini.HandleFunc("/info", infoPage)

	gemini.HandleFunc("/certs", certPage)
    
	gemini.HandleFunc("/uploads/show", func (w *gemini.Response, r *gemini.Request) {  uploads(w, r, "show")})
	gemini.HandleFunc("/uploads/post", func (w *gemini.Response, r *gemini.Request) {  uploads(w, r, "post")})
	gemini.HandleFunc("/uploads", func (w *gemini.Response, r *gemini.Request) {  uploads(w, r, "show")})

	//some routes for handling edit/display of the message of the day
	gemini.HandleFunc("/motd/edit", func (w *gemini.Response, r *gemini.Request) {  messageOTD(w, r, "edit")})
	gemini.HandleFunc("/motd/post", func (w *gemini.Response, r *gemini.Request) {  messageOTD(w, r, "post")})
	gemini.HandleFunc("/motd/show", func (w *gemini.Response, r *gemini.Request) {  messageOTD(w, r, "show")})
	gemini.HandleFunc("/motd", func (w *gemini.Response, r *gemini.Request) {  messageOTD(w, r, "show")})

	//some routes for handling a very simple message board
	gemini.HandleFunc("/board/post", func (w *gemini.Response, r *gemini.Request) {  messageBoard(w, r, "post")})
	gemini.HandleFunc("/board/show", func (w *gemini.Response, r *gemini.Request) {  messageBoard(w, r, "show")})
	gemini.HandleFunc("/board", func (w *gemini.Response, r *gemini.Request) {  messageBoard(w, r, "show")})

	//put the generic one last, otherwise it will take precedence over others
    //file handling module is. FileServer module only really good for serving
    //server URL root at present (needs some improvement)
	gemini.Handle("/", gemini.FileServer(*root))

	log.Fatal(gemini.ListenAndServeTLS(*bind, *crt, *key))
}

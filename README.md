# genimigem

A dynamic Gemini and Nimigem server.

Genimigem is a demonstrator server in Go that implements both gemini and nimigem protocols. It supports CGI scripts which allow server applications to be rapidly developed. 

It is a wrapper around the https://github.com/LukeEmmet/net-gemini server framework which provides the underlying gemini and nimigem protocol handling.

## What is Nimigem

Nimigem is a companion protocol for Gemini, allowing simple text based content submission and client integration. It is designed to fill the gap left by Gemini for non-idempotent client content submission (somewhat akin to HTTP/POST), since Gemini requests only support query based parameters (like HTTP/GET).

## Static files

docs/ folder contains home page and static files, served from gemini://server/docs/*

## CGI scripting

cgi-bin/ folder for CGI scripts, served and executed from gemini://server/cgi-bin/*

## Server info

Server info is served from gemini://server/info

## Usage

Command line parameters:

  -bind string
        bind to (default "localhost:1965")
  -cgi string
        cgi directory (default "cgi-bin")
  -crt string
        path to cert
  -docs string
        docs directory (default "docs")
  -key string
        path to cert key

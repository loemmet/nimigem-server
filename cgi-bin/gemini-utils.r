REBOL [

]

gmi-preformat: funct [data label] [
    rejoin [
        newline
        {# } label  newline
        {```} newline
        mold data newline
        {```} newline
    ]
]

preformat-escape: funct [content] [
    ;---replace all \n``` with \n#```
    ;---where # is a zero width space - effectively escapes the line and is required by nimigem for editing
    escaped: join (to-string #{0A}) copy content    ;--prepend \n in case starts with preformatted marker already
    replace/all escaped (to-string #{0A606060}) (to-string #{0AE2808B606060})     ;escape preformatted areas with a zero width space prefix
    next escaped    ;-skip beyond the inserted \n
]


url-decode: use [as-is hex space][
    as-is: charset ["-._~" #"0" - #"9" #"A" - #"Z" #"a" - #"z"]
    reserved: charset [":/?#[]@!$&'()*+,;="]
    
    hex: charset [#"0" - #"9" #"a" - #"f" #"A" - #"F"]

    funct [text [any-string!]][
        either parse/all text: to binary! text [
            copy text any [
                  some as-is 
                | some reserved
                | change "%0D%0A" "^/" ; de-crlf
                | remove ["%" copy text 2 hex] (text: debase/base text 16) insert text
            ]
        ][to string! text][none]
    ]
]

;---REBOL bug workaround - we have to be careful trying to return CR/LF - REBOL sometimes gets it wrong if we use interpreted characters like ^/^M or similar
;---so send the literal CRLF bytes on Windows
send-gemini-header: funct [status meta] [
    
    either system/version/4 = 3 [
        ;--for some reason the simple approach does not seem to work on Windows (not sure why)
        prin rejoin [ status " " meta  ( to-string #{0D0A})]
    ] [        
        ;--for unix!
        print rejoin [ status " " meta  "^M"]
    ]
    
]


cgi-post-content: funct [] [
     
    ;--if content is short you can just use: data-post:  to string! read system/ports/input
    ;--but for longer content we must read the port directly
    ;---this approach taken from http://stackoverflow.com/questions/3033936/reading-large-binary-files-fails-in-rebol
    len: load any [ (get-env "CONTENT_LENGTH") "0" ]
    data-post: make string! ( len + 10 )
    chunk-size: 32000

    in-port: open/read system/ports/input

    attempt [
        while [not empty? data: read/part in-port chunk-size] [
            append data-post  to-string data
        ]
    ]
    close in-port
    
    :data-post

]

;--return decoded query string
cgi-get-content: does [
    get-query-string
]

get-url-authority: does [
    get-env "SERVER_NAME"
]

get-raw-query: does [
    get-env "QUERY_STRING"
]

get-path-info: does [
    get-env "PATH_INFO"
]

get-query-string: does [

    query: copy ""
    
    ;---get any query string data too
    ;--this version of molly-brown returns raw query in query string, so decode it
    if (error? try [query:  url-decode get-env "QUERY_STRING"]) [
        query: copy ""
    ]

    if unset? query [query: copy ""]
    
    :query
]
REBOL []

safe-snip-name: funct [s ] [
    ;--effectively does a few things:
    ;       space normaliser - removes non latin ascii chars from string
    ;       prevents use of non-url chars in possible urls
    ;       allows snips to be specified case insensitively as we only look up safe snip name
    ;       But this means all titles are stripped of any non-latin chars.
    
    result: copy ""
    working-s:  lowercase to-string copy s
    
    ;--define opt-in list, any sequence of all others collapsed into hyphen
    safe-chars: charset "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    
    foreach char working-s [
        either (find safe-chars char) [
            space-flag: off
            append result  lowercase to-string char
        ] [
            if not space-flag [
                append result #"-"
            ]
            space-flag: on
        ]
    ]
    
    if (1 < length? result) and (#"-" = first result) [
        result: next result     ;remove leading hypen, if any
    ]
    
    :result
]